/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package randarray;

/**
 *
 * @author ipd
 */
public class RandArray {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int intArray[] = new int[10];

        {
            int i = 2, j = 3;
            
            int k = i++ + j;
            
            System.out.printf("k = %d, i = %d, j = %d\n", k, i, j);
        }
        
        double num = 1.0;
        for (int i=0; i<10; i++) {
            num -= 0.1;
        }
        System.out.printf("Result is: %.2f\n", num);

        for (double v=-10; v<=-9; v+=0.1) {
            System.out.printf("v = %-10.3f is the number\n", v);
        }
        
        double pi = 3.141592;
        String piAsString = String.format("PI = %7.3f", pi);
        System.out.println("PI is " + piAsString);
        
    }

}
